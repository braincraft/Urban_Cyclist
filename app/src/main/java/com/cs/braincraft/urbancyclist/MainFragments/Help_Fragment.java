package com.cs.braincraft.urbancyclist.MainFragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cs.braincraft.urbancyclist.R;

/**
 * Created by braincraft on 3/6/15.
 */
public class Help_Fragment extends Fragment {
    View rootview;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        rootview=inflater.inflate(R.layout.help_layout,container,false);
        //Setting font for the introduction
        Typeface myTypeface= Typeface.createFromAsset(getActivity().getAssets(),"Vonique 64 Bold.ttf");
        TextView title=(TextView)rootview.findViewById(R.id.help_title);
        title.setTypeface(myTypeface);
        TextView content=(TextView)rootview.findViewById(R.id.help_content);
        //content.setTypeface(myTypeface);
        return  rootview;
    }
}
