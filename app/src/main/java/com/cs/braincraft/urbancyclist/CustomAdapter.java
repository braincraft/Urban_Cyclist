package com.cs.braincraft.urbancyclist;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.braincraft.urbancyclist.MainFragments.Iterinary_Fragment;
import com.cs.braincraft.urbancyclist.MainFragments.itinerary.DBHandler;
import com.cs.braincraft.urbancyclist.MainFragments.itinerary.itinerary;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.ShareOpenGraphAction;
import com.facebook.share.model.ShareOpenGraphContent;
import com.facebook.share.model.ShareOpenGraphObject;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by braincraft on 3/23/15.
 */


public class CustomAdapter extends ArrayAdapter<itinerary> {

    private ArrayList<itinerary> items;
    private int layoutResourceId;
    private Context context;
    private int type;
    private TextView date;
    private TextView route;
    private TextView distance;

    private List<itinerary> myList = new ArrayList<itinerary>();
    private ShareDialog shareDialog;
    private ShareLinkContent content;
    public CustomAdapter(Context context, List<itinerary> myList) {
        super(context, 0, myList);
        this.type = 0;
        this.myList = myList;
    }


    public CustomAdapter(Context context,ArrayList<itinerary> resource) {
        super(context,R.layout.custom_row,resource);
        this.layoutResourceId=R.layout.custom_row;
        this.context=context;
        this.items=resource;


    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent){
        LayoutInflater table_inflater = LayoutInflater.from(getContext());
        final View customView = table_inflater.inflate(R.layout.custom_row,parent,false);

        itinerary single_row = getItem(position);
        route=(TextView) customView.findViewById(R.id.Route);
        date = (TextView) customView.findViewById(R.id.Date);
        distance = (TextView) customView.findViewById(R.id.Distance);
        Button delete_button = (Button) customView.findViewById(R.id.delete_button);
        delete_button.setTag(position);

        route.setText(single_row.get_Route());
        date.setText(single_row.get_Date());
        distance.setText(String.valueOf(single_row.get_Distance()));
        delete_button.setText("Delete");

        delete_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer tag = (Integer) v.getTag();


                itinerary item = getItem(tag);
                String date2 = item.get_Date();
                String route2 = item.get_Route();
                Double distance2 = item.get_Distance();
                //Toast.makeText(context, "Deleted Row with Info:\nDate: " + date2 + "\nRoute: " + route2 + "\nDistance: " + distance2, Toast.LENGTH_LONG).show();

                DBHandler mDbHelper = new DBHandler(context);

                String t_date = "\"" + date2 + "\"";
                String t_route = "\"" + route2 + "\"";
                SQLiteDatabase db = mDbHelper.getWritableDatabase();
                String query = "DELETE FROM " + DBHandler.TABLE_ITINERARY + " WHERE " + DBHandler.KEY_DATE + "=" + t_date + " AND " + DBHandler.KEY_ROUTE + "=" + t_route + " AND " + DBHandler.KEY_DISTANCE + "=" + distance2;

                db.execSQL(query);
                db.close();
                deleteItem(tag.intValue());
                notifyDataSetChanged();
                //Toast.makeText(context, "Chosen Row: " + v.getTag(), Toast.LENGTH_SHORT).show();
            }
        });


        Resources resources = context.getResources();
        String t_string=new String();
        if(route.getText().equals("Laguna Loop"))
            t_string="https://www.google.com/maps/d/edit?mid=z0faN37ER7eQ.kUP8y3VEaKco&usp=sharing";
        else if(route.getText().equals("Puning Cave Bulacan"))
            t_string="https://www.google.com/maps/d/edit?mid=z0faN37ER7eQ.k-To2tYGWf5A&usp=sharing";
        else if(route.getText().equals("Mt. Samat"))
            t_string="https://www.google.com/maps/d/edit?mid=z0faN37ER7eQ.k4qjcVnHLONI&usp=sharing";
        else if(route.getText().equals("Tagaytay to Calamba"))
            t_string="https://www.google.com/maps/d/edit?mid=z0faN37ER7eQ.kUQXrTdXlFXY&usp=sharing";
        else if(route.getText().equals("Manila to Clark"))
            t_string="https://www.google.com/maps/d/edit?mid=z0faN37ER7eQ.k6X1WFbjuTRo&usp=sharing";
        content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse(t_string))
                .setContentTitle("Urban Cyclist")
                .setContentDescription("[" + String.valueOf(date.getText()) + "] I have navigated through " + route.getText() + " (" + String.valueOf(distance.getText()) + "km ).")
                .build();




        ShareButton shareButton = (ShareButton) customView.findViewById(R.id.shareButton);

        shareButton.setShareContent(content);

        return customView;

    }
    private void deleteItem(int position)
    {
        itinerary item = getItem(position);
        remove(getItem(position));
    }

}
