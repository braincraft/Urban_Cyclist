package com.cs.braincraft.urbancyclist.MainFragments;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cs.braincraft.urbancyclist.CustomAdapter;
import com.cs.braincraft.urbancyclist.MainActivity;
import com.cs.braincraft.urbancyclist.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


/**
 * Created by braincraft on 3/6/15.
 */
public class Iterinary_Fragment extends Fragment {
    View rootview;





    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        rootview=inflater.inflate(R.layout.iterinary_layout,container,false);

        Typeface myTypeface= Typeface.createFromAsset(getActivity().getAssets(),"Opificio.ttf");
        TextView title=(TextView)rootview.findViewById(R.id.textView);
        title.setTypeface(myTypeface);
        //===================================================================================================
        //Itinerary Row
        //===================================================================================================
        ((MainActivity)getActivity()).itinerary_row = (ListView) rootview.findViewById(R.id.itinerary_list);

        ((MainActivity)getActivity()).entries=((MainActivity)getActivity()).getAllElements();

        Collections.reverse(((MainActivity)getActivity()).entries);



        ((MainActivity)getActivity()).itinerary_adapter= new CustomAdapter(getActivity(),((MainActivity)getActivity()).entries
        );
        ((MainActivity)getActivity()).itinerary_row.setAdapter(((MainActivity)getActivity()).itinerary_adapter);


        //===================================================================================================
        //End of "Itinerary Row"
        //===================================================================================================



        return  rootview;
    }









}
