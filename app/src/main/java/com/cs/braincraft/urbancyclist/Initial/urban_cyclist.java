package com.cs.braincraft.urbancyclist.Initial;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cs.braincraft.urbancyclist.R;

/**
 * Created by braincraft on 6/8/15.
 */
public class urban_cyclist extends Fragment{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.urban_cyclist, container, false);

        return rootView;
    }
}
