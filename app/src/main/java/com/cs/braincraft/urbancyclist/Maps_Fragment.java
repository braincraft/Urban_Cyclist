package com.cs.braincraft.urbancyclist;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.cs.braincraft.urbancyclist.MainFragments.itinerary.DBHandler;
import com.cs.braincraft.urbancyclist.MainFragments.itinerary.Places;
import com.cs.braincraft.urbancyclist.MainFragments.itinerary.itinerary;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;

import java.text.BreakIterator;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by braincraft on 3/6/15.
 */
public class Maps_Fragment extends Fragment implements OnMapReadyCallback{

    private Button start_button;
    private Button stop_button;

    private Spinner choose_loc;
    private GoogleApiClient mGoogleApiClient;
    private BreakIterator mLatitudeText;
    private BreakIterator mLongitudeText;
    private Marker laguna;
    private Marker bulacan;
    private Marker manila;
    private Marker tagaytay;
    private Marker samat;
    private Marker start;
    private Marker end;
    private Polyline laguna_line;
    private Polyline bulacan_line;
    private Polyline manila_line;
    private Polyline tagaytay_line;
    private Polyline samat_line;
    private GPStracker gps = new GPStracker(getActivity());
    private double distance=0;
    private static View view;
    private Marker[][] stop_over = new Marker[5][10];
    public static int LAGUNA=0;
    public static int BULACAN=1;
    public static int SAMAT=2;
    public static int TAGAYTAY=3;
    public static int MANILA=4;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(view!=null)
        {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.maps_layout, container, false);
            SupportMapFragment mapFrag = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
            start_button = (Button) view.findViewById(R.id.start);
            stop_button = (Button) view.findViewById(R.id.stop);
            mapFrag.getMapAsync(this);
        }catch(InflateException e){}
        return view;
    }



    /*
    Synchronizing the Map
     */
    //=======================================================================================================
    @Override
    public void onMapReady(final GoogleMap googleMap) {

        googleMap.setMyLocationEnabled(true);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(14.5975732,120.9045156),9));
         /*
        Setting the routes
         */
        //=================================================================================================
        final Places temp = new Places();
        bulacan_line=googleMap.addPolyline(temp.bulacan_line);
        samat_line = googleMap.addPolyline(temp.samat_line);
        laguna_line = googleMap.addPolyline(temp.laguna_line);
        tagaytay_line = googleMap.addPolyline(temp.tagaytay_line);
        manila_line=googleMap.addPolyline(temp.manila_line);


        //Set them all to invisible at start
        bulacan_line.setVisible(false);
        manila_line.setVisible(false);
        tagaytay_line.setVisible(false);
        laguna_line.setVisible(false);
        samat_line.setVisible(false);
        //=================================================================================================
        //End of Section " Setting the routes"
        //=================================================================================================



        /*
        Setting up markers for each routes on the options menu
         */
        //=================================================================================================

        //Marker for laguna loop
        laguna=googleMap.addMarker(new MarkerOptions().position(new LatLng(14.4163928,121.04513168)));
        laguna.setTitle("Laguna Loop");

        //Marker for bulacan cave
        bulacan=googleMap.addMarker(new MarkerOptions().position(new LatLng(14.81936205,121.06075287)));
        bulacan.setTitle("Puning Cave Bulacan");

        //Marker for Mt.Samat
        samat=googleMap.addMarker(new MarkerOptions().position(new LatLng(14.605719,120.508926)));
        samat.setTitle("Mt. Samat");

        //Marker  for Tagaytay road
        tagaytay=googleMap.addMarker(new MarkerOptions().position(new LatLng(14.11510093,120.96179008)));
        tagaytay.setTitle("Tagaytay to Calamba Rd.");

        //Marker for Manila road
        manila=googleMap.addMarker(new MarkerOptions().position(new LatLng(14.453454, 120.916842)));
        manila.setTitle("Manila to Clark");



        //=================================================================================================
        //End of Section "Setting up markers for each routes on the options menu"
        //=================================================================================================









        /*
         Start and final markers on each routes
        */
        //=================================================================================================
        start = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.453454, 120.916842)).title("Start").icon(BitmapDescriptorFactory.fromResource(R.drawable.start_flag)));
        end = googleMap.addMarker(new MarkerOptions().position(new LatLng(15.20049387,120.57588995)).title("End").icon(BitmapDescriptorFactory.fromResource(R.drawable.end_flag)));
        start.setVisible(false);
        end.setVisible(false);
        //=================================================================================================
        //End of Section "Start and final markers on each routes "
        //=================================================================================================




        /*
         Initialize all stop over markers
        */
        //=================================================================================================
        int x=0;
        stop_over[LAGUNA][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.4490517,121.043118)).icon(BitmapDescriptorFactory.fromResource(R.drawable.water)).visible(false));
        stop_over[LAGUNA][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.5193905, 121.0491856)).icon(BitmapDescriptorFactory.fromResource(R.drawable.water)).visible(false));
        stop_over[LAGUNA][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.5278654,121.0848053)).icon(BitmapDescriptorFactory.fromResource(R.drawable.food)).visible(false));
        stop_over[LAGUNA][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.4790268, 121.1903819)).icon(BitmapDescriptorFactory.fromResource(R.drawable.water)).visible(false));
        stop_over[LAGUNA][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.4782061,121.2049087)).icon(BitmapDescriptorFactory.fromResource(R.drawable.food)).visible(false));
        stop_over[LAGUNA][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.3156354,121.212891)).icon(BitmapDescriptorFactory.fromResource(R.drawable.food)).visible(false));
        stop_over[LAGUNA][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.3950028,121.4469513)).icon(BitmapDescriptorFactory.fromResource(R.drawable.water)).visible(false));
        stop_over[LAGUNA][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.2707632,121.3927063)).icon(BitmapDescriptorFactory.fromResource(R.drawable.water)).visible(false));
        stop_over[LAGUNA][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.174076, 121.257350)).icon(BitmapDescriptorFactory.fromResource(R.drawable.food)).visible(false));
        stop_over[LAGUNA][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.248802,121.1279186)).icon(BitmapDescriptorFactory.fromResource(R.drawable.water)).visible(false));

        x=0;
        stop_over[BULACAN][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.685869, 121.078665)).icon(BitmapDescriptorFactory.fromResource(R.drawable.water)).visible(false));
        stop_over[BULACAN][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.702194, 121.075575)).icon(BitmapDescriptorFactory.fromResource(R.drawable.food)).visible(false));
        stop_over[BULACAN][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.729165, 121.056461)).icon(BitmapDescriptorFactory.fromResource(R.drawable.food)).visible(false));
        stop_over[BULACAN][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.762491, 121.084399)).icon(BitmapDescriptorFactory.fromResource(R.drawable.water)).visible(false));
        stop_over[BULACAN][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.798841, 121.066203)).icon(BitmapDescriptorFactory.fromResource(R.drawable.water)).visible(false));
        stop_over[BULACAN][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.862397, 121.089463)).icon(BitmapDescriptorFactory.fromResource(R.drawable.food)).visible(false));
        stop_over[BULACAN][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.873680, 121.035562)).icon(BitmapDescriptorFactory.fromResource(R.drawable.water)).visible(false));
        stop_over[BULACAN][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.959272, 121.051698)).icon(BitmapDescriptorFactory.fromResource(R.drawable.food)).visible(false));
        stop_over[BULACAN][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.905532, 121.031442)).icon(BitmapDescriptorFactory.fromResource(R.drawable.food)).visible(false));
        stop_over[BULACAN][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.981162, 121.079850)).icon(BitmapDescriptorFactory.fromResource(R.drawable.water)).visible(false));

        x=0;
        stop_over[SAMAT][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.600388, 120.479592)).icon(BitmapDescriptorFactory.fromResource(R.drawable.water)).visible(false));
        stop_over[SAMAT][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.652211, 120.443200)).icon(BitmapDescriptorFactory.fromResource(R.drawable.food)).visible(false));
        stop_over[SAMAT][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.834157, 120.448693)).icon(BitmapDescriptorFactory.fromResource(R.drawable.food)).visible(false));
        stop_over[SAMAT][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.924410, 120.565423)).icon(BitmapDescriptorFactory.fromResource(R.drawable.water)).visible(false));
        stop_over[SAMAT][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(15.000033, 120.620355)).icon(BitmapDescriptorFactory.fromResource(R.drawable.water)).visible(false));
        stop_over[SAMAT][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(15.092868, 120.702752)).icon(BitmapDescriptorFactory.fromResource(R.drawable.food)).visible(false));
        stop_over[SAMAT][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(15.180360, 120.739831)).icon(BitmapDescriptorFactory.fromResource(R.drawable.water)).visible(false));
        stop_over[SAMAT][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(15.122036, 120.826348)).icon(BitmapDescriptorFactory.fromResource(R.drawable.food)).visible(false));
        stop_over[SAMAT][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.850087, 120.863427)).icon(BitmapDescriptorFactory.fromResource(R.drawable.food)).visible(false));
        stop_over[SAMAT][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.758475, 120.954064)).icon(BitmapDescriptorFactory.fromResource(R.drawable.water)).visible(false));

        x=0;
        stop_over[TAGAYTAY][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.123967, 120.966715)).icon(BitmapDescriptorFactory.fromResource(R.drawable.water)).visible(false));
        stop_over[TAGAYTAY][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.128295, 120.982851)).icon(BitmapDescriptorFactory.fromResource(R.drawable.food)).visible(false));
        stop_over[TAGAYTAY][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.137606, 121.016271)).icon(BitmapDescriptorFactory.fromResource(R.drawable.food)).visible(false));
        stop_over[TAGAYTAY][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.133153, 121.006680)).icon(BitmapDescriptorFactory.fromResource(R.drawable.water)).visible(false));
        stop_over[TAGAYTAY][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.127285, 120.996702)).icon(BitmapDescriptorFactory.fromResource(R.drawable.water)).visible(false));
        stop_over[TAGAYTAY][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.132445, 121.003912)).icon(BitmapDescriptorFactory.fromResource(R.drawable.food)).visible(false));
        stop_over[TAGAYTAY][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.133694, 121.010177)).icon(BitmapDescriptorFactory.fromResource(R.drawable.water)).visible(false));
        stop_over[TAGAYTAY][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.137606, 121.011122)).icon(BitmapDescriptorFactory.fromResource(R.drawable.food)).visible(false));
        stop_over[TAGAYTAY][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.136898, 121.012323)).icon(BitmapDescriptorFactory.fromResource(R.drawable.food)).visible(false));
        stop_over[TAGAYTAY][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.135858, 121.009705)).icon(BitmapDescriptorFactory.fromResource(R.drawable.water)).visible(false));

        x=0;
        stop_over[MANILA][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.475690, 120.987719)).icon(BitmapDescriptorFactory.fromResource(R.drawable.water)).visible(false));
        stop_over[MANILA][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(15.128978, 120.633982)).icon(BitmapDescriptorFactory.fromResource(R.drawable.food)).visible(false));
        stop_over[MANILA][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.547482, 121.003512)).icon(BitmapDescriptorFactory.fromResource(R.drawable.food)).visible(false));
        stop_over[MANILA][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(15.004326, 120.749339)).icon(BitmapDescriptorFactory.fromResource(R.drawable.water)).visible(false));
        stop_over[MANILA][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.642504, 120.980166)).icon(BitmapDescriptorFactory.fromResource(R.drawable.water)).visible(false));
        stop_over[MANILA][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.683025, 121.002826)).icon(BitmapDescriptorFactory.fromResource(R.drawable.food)).visible(false));
        stop_over[MANILA][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.748773, 120.975360)).icon(BitmapDescriptorFactory.fromResource(R.drawable.water)).visible(false));
        stop_over[MANILA][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.825122, 120.908068)).icon(BitmapDescriptorFactory.fromResource(R.drawable.food)).visible(false));
        stop_over[MANILA][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(14.883583, 120.828303)).icon(BitmapDescriptorFactory.fromResource(R.drawable.food)).visible(false));
        stop_over[MANILA][x++] = googleMap.addMarker(new MarkerOptions().position(new LatLng(15.029527, 120.735606)).icon(BitmapDescriptorFactory.fromResource(R.drawable.water)).visible(false));
        //=================================================================================================
        //End of Section "Start and final markers on each routes "
        //=================================================================================================



        /*
        Top Option List
         */
        //=======================================================================================================
        choose_loc=(Spinner)view.findViewById(R.id.locations);
        choose_loc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int item=choose_loc.getSelectedItemPosition();
                //if the selected route is not "Choose Route"
                if (item!=0)
                {
                    isGPSon();
                    //Hide all the initial markers
                    laguna.setVisible(false);
                    bulacan.setVisible(false);
                    samat.setVisible(false);
                    tagaytay.setVisible(false);
                    manila.setVisible(false);
                    start.setVisible(true);
                    end.setVisible(true);
                    start_button.setEnabled(true);
                    stop_button.setEnabled(false);
                    hide_Stop();

                    //if Option is Manila
                    if(item==5) {

                        manila_line.setVisible(true);
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(14.84288438,120.88683844), 9));
                        start.setPosition(new LatLng(14.453454, 120.916842));
                        end.setPosition(new LatLng(15.20049387,120.57588995));

                        int x;
                        for(x=0;x<10;x++)
                            stop_over[MANILA][x].setVisible(true);
                        bulacan_line.setVisible(false);
                        samat_line.setVisible(false);
                        tagaytay_line.setVisible(false);
                        laguna_line.setVisible(false);
                    }

                    //if Option is tagaytay
                    else if(item==4) {
                        tagaytay_line.setVisible(true);
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(14.12742001, 120.99715233), 12));
                        start.setPosition(new LatLng(14.11547551,120.96202612));
                        end.setPosition(new LatLng(14.14009735,121.01972848));

                        int x;
                        for(x=0;x<10;x++)
                            stop_over[TAGAYTAY][x].setVisible(true);
                        bulacan_line.setVisible(false);
                        samat_line.setVisible(false);
                        manila_line.setVisible(false);
                        laguna_line.setVisible(false);
                    }

                    //if Option is samat
                    else if(item==3) {
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(14.64471105, 120.75828552), 8));
                        samat_line.setVisible(true);
                        start.setPosition(new LatLng(14.65176952,121.04650497));
                        end.setPosition(new LatLng(14.60679899,120.51023483));

                        int x;
                        for(x=0;x<10;x++)
                            stop_over[SAMAT][x].setVisible(true);
                        bulacan_line.setVisible(false);
                        manila_line.setVisible(false);
                        tagaytay_line.setVisible(false);
                        laguna_line.setVisible(false);
                    }

                    //if Option is bulacan
                    else if(item==2) {
                        bulacan_line.setVisible(true);
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(14.77753826,121.0717392), 10));
                        start.setPosition(new LatLng(14.65455133,121.05856419));
                        end.setPosition(new LatLng(14.6261088,121.08306885));


                        int x;
                        for(x=0;x<10;x++)
                            stop_over[BULACAN][x].setVisible(true);
                        samat_line.setVisible(false);
                        manila_line.setVisible(false);
                        tagaytay_line.setVisible(false);
                        laguna_line.setVisible(false);
                    }

                    //if option is laguna
                    else if(item==1) {
                        laguna_line.setVisible(true);
                        start.setPosition(new LatLng(14.37968874, 121.04551792));
                        end.setPosition(new LatLng(14.37968874, 121.04551792));
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(14.34821739,121.27052307), 10));

                        int x;
                        for(x=0;x<10;x++)
                            stop_over[LAGUNA][x].setVisible(true);
                        samat_line.setVisible(false);
                        manila_line.setVisible(false);
                        tagaytay_line.setVisible(false);
                        bulacan_line.setVisible(false);
                    }

                    start_button.setEnabled(true);
                    stop_button.setEnabled(false);

                }
                else
                {


                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(14.5975732,120.9045156),9));
                    samat_line.setVisible(false);
                    manila_line.setVisible(false);
                    tagaytay_line.setVisible(false);
                    laguna_line.setVisible(false);
                    bulacan_line.setVisible(false);
                    start.setVisible(false);
                    end.setVisible(false);


                    laguna.setVisible(true);
                    bulacan.setVisible(true);
                    samat.setVisible(true);
                    tagaytay.setVisible(true);
                    manila.setVisible(true);



                    start_button.setEnabled(false);
                    start_button.setText("Start");
                    stop_button.setEnabled(false);
                    hide_Stop();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //=======================================================================================================
        //End of Section "Top Option Bar"
        //=======================================================================================================




        /*
        Start Button and Stop Button for tracking
         */
        //=======================================================================================================
        start_button.setEnabled(false);
        stop_button.setEnabled(false);
        start_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(start_button.getText()=="Start") {
                    gps= new GPStracker(getActivity());
                    start_button.setText("Pause");
                    stop_button.setEnabled(true);
                    Toast.makeText(getActivity(),"Recording",Toast.LENGTH_SHORT).show();
                    distance=0;
                    gps.setDistance(0);
                    try {
                        gps.last_latitude = gps.locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLatitude();
                        gps.last_longitude = gps.locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLongitude();
                    }
                    catch(Exception e){}
                    gps.locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,5000,0,gps);
                }
                else if (start_button.getText()=="Pause")
                {
                    start_button.setText("Resume");
                    stop_button.setEnabled(true);
                    distance=gps.getDistance();
                    gps.stopUsingGPS();
                    Toast.makeText(getActivity(),"Distance Travelled: "+distance,Toast.LENGTH_SHORT).show();
                }
                else
                {
                    gps= new GPStracker(getActivity());
                    start_button.setText("Pause");
                    stop_button.setEnabled(true);
                    Toast.makeText(getActivity(),"Recording",Toast.LENGTH_SHORT).show();
                    gps.setDistance(distance);
                    try {
                        gps.last_latitude = gps.locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLatitude();
                        gps.last_longitude = gps.locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLongitude();
                    }
                    catch(Exception e){}
                    gps.locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,5000,0,gps);
                }


            }
        });

        stop_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stop_button.setEnabled(false);
                start_button.setText("Start");
                distance=gps.getDistance();
                gps.stopUsingGPS();

                Toast.makeText(getActivity(),"Distance Travelled: "+distance,Toast.LENGTH_SHORT).show();


                //add new list entry on the itinerary
                DBHandler mDbHelper = new DBHandler(getActivity().getBaseContext());
                SQLiteDatabase db = mDbHelper.getWritableDatabase();


                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                Date date = new Date();
                ContentValues values = new ContentValues();
                String v_route = choose_loc.getSelectedItem().toString();
                values.put(DBHandler.KEY_DATE, (String) dateFormat.format(date));
                values.put(DBHandler.KEY_ROUTE, v_route);
                values.put(DBHandler.KEY_DISTANCE, distance);

                long newRowId;
                newRowId = db.insert(
                        DBHandler.TABLE_ITINERARY,
                        null,
                        values);

                db.close();
                ((MainActivity)getActivity()).itinerary_adapter.insert(new itinerary((String) dateFormat.format(date),v_route,distance),0);
                ((MainActivity)getActivity()).update();

                distance = 0;


                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(14.5975732,120.9045156),9));
                samat_line.setVisible(false);
                manila_line.setVisible(false);
                tagaytay_line.setVisible(false);
                laguna_line.setVisible(false);
                bulacan_line.setVisible(false);
                start.setVisible(false);
                end.setVisible(false);


                laguna.setVisible(true);
                bulacan.setVisible(true);
                samat.setVisible(true);
                tagaytay.setVisible(true);
                manila.setVisible(true);



                start_button.setEnabled(false);
                start_button.setText("Start");
                stop_button.setEnabled(false);
                choose_loc.setSelection(0);
            }
        });

        //=======================================================================================================
        //End of Section "Start Button and Stop Button for tracking"
        //=======================================================================================================

    }



    //=======================================================================================================
    //End of Section "Sychronizing the map"
    //=======================================================================================================
    public void isGPSon(){
        String provider = Settings.Secure.getString(getActivity().getContentResolver(),
                Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if(!provider.equals("")){
        }else{
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("GPS Settings");
            alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");
            alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    getActivity().startActivity(intent);
                }
            });

            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();
        }
    }

    public void hide_Stop(){
        int x=0;
        for(x=0;x<10;x++) {
            stop_over[LAGUNA][x].setVisible(false);
            stop_over[BULACAN][x].setVisible(false);
            stop_over[SAMAT][x].setVisible(false);
            stop_over[TAGAYTAY][x].setVisible(false);
            stop_over[MANILA][x].setVisible(false);
        }


    }
}