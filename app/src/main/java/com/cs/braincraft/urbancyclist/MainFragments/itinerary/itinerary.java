package com.cs.braincraft.urbancyclist.MainFragments.itinerary;

/**
 * Created by braincraft on 3/23/15.
 */
public class itinerary {
    private String _Date,_Route;
    private double _Distance;
    public itinerary(String date, String route, double distance){
        _Date=date;
        _Route=route;
        _Distance=distance;

    }

    public String get_Date(){
        return _Date;
    }
    public String get_Route(){
        return _Route;
    }
    public double get_Distance(){
        return _Distance;
    }



}
