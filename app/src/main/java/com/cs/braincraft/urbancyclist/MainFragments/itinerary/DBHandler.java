package com.cs.braincraft.urbancyclist.MainFragments.itinerary;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.cs.braincraft.urbancyclist.MainFragments.itinerary.itinerary;

/**
 * Created by braincraft on 3/23/15.
 */
public class DBHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "itineraryManager",
        TABLE_ITINERARY = "itinerary", KEY_ID = "id", KEY_DATE = "idate",
    KEY_ROUTE = "route", KEY_DISTANCE = "distance";

    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_ITINERARY + "("+"idate text, route text, distance real "+")");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS" + TABLE_ITINERARY);
        onCreate(db);
    }

    public void createItinerary(itinerary t_itinerary){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_DATE, t_itinerary.get_Date());
        values.put(KEY_ROUTE,t_itinerary.get_Route());
        values.put(KEY_DISTANCE,t_itinerary.get_Distance());

        db.insert(TABLE_ITINERARY,null,values);
        db.close();
    }


}
