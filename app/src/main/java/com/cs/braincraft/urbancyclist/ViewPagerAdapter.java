package com.cs.braincraft.urbancyclist;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.cs.braincraft.urbancyclist.MainFragments.About_Fragment;
import com.cs.braincraft.urbancyclist.MainFragments.Credits_Fragment;
import com.cs.braincraft.urbancyclist.MainFragments.Help_Fragment;
import com.cs.braincraft.urbancyclist.MainFragments.Iterinary_Fragment;


/**
 * Created by braincraft on 4/2/15.
 */
    public class ViewPagerAdapter extends FragmentPagerAdapter {

        final int PAGE_COUNT = 5;
        // Tab Titles
        private String tabtitles[] = new String[]{"Itinerary", "Maps", "Help", "About", "Credits"};
        Context context;

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }



        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new Iterinary_Fragment();



                case 1:
                    return new Maps_Fragment();



                case 2:
                    return new Help_Fragment();


                case 3:
                    return new About_Fragment();



                case 4:
                    return new Credits_Fragment();

            }
            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabtitles[position];
        }

    }
