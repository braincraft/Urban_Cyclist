package com.cs.braincraft.urbancyclist;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.cs.braincraft.urbancyclist.MainFragments.itinerary.DBHandler;
import com.cs.braincraft.urbancyclist.MainFragments.itinerary.itinerary;
import com.facebook.FacebookSdk;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class MainActivity extends ActionBarActivity{

    public static ViewPager viewPager;
    public ListView itinerary_row;
    public static ArrayList<itinerary> entries;
    public static ArrayAdapter itinerary_adapter;
    private boolean doubleBackToExitPressedOnce;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        // Get the view from activity_main.xml
        setContentView(R.layout.activity_main);


        // Locate the viewpager in activity_main.xml
        viewPager = (ViewPager) findViewById(R.id.pager);
        android.app.ActionBar actionBar = getActionBar();

            // Set the ViewPagerAdapter into ViewPager
            viewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));


        SharedPreferences isFirstRun = getSharedPreferences("PREFERENCE", MODE_PRIVATE);


        if (isFirstRun.getBoolean("isFirstRun", true)) {
            //show start activity
            startActivity(new Intent(this, ScreenSlidePagerActivity.class));
            SharedPreferences.Editor initialize = isFirstRun.edit();
            initialize.putBoolean("isFirstRun",false);
            initialize.commit();

            DBHandler mDbHelper = new DBHandler(this.getBaseContext());
            SQLiteDatabase db = mDbHelper.getWritableDatabase();


            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            Date date = new Date();
            ContentValues values = new ContentValues();
            String v_route="Sample Output 1";
            values.put(DBHandler.KEY_DATE, (String)dateFormat.format(date));
            values.put(DBHandler.KEY_ROUTE, v_route);
            values.put(DBHandler.KEY_DISTANCE, 12345.6789);

            long newRowId;
            newRowId = db.insert(
                    DBHandler.TABLE_ITINERARY,
                    null,
                    values);

            ContentValues values2 = new ContentValues();
            String v_route2="Sample Output2";
            values2.put(DBHandler.KEY_DATE, (String)dateFormat.format(date));
            values2.put(DBHandler.KEY_ROUTE, v_route2);
            values2.put(DBHandler.KEY_DISTANCE, 12345.67891);
            newRowId = db.insert(
                    DBHandler.TABLE_ITINERARY,
                    null,
                    values2);

            db.close();



        }

        //startActivity(new Intent(this, ScreenSlidePagerActivity.class));
    }


    public void update(){
        entries=getAllElements();
        Collections.reverse(entries);
        itinerary_adapter.notifyDataSetChanged();
    }

    public ArrayList<itinerary> getAllElements() {

        ArrayList<itinerary> list = new ArrayList<itinerary>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + DBHandler.TABLE_ITINERARY;

        DBHandler mDbHelper = new DBHandler(getBaseContext());
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        try {

            Cursor cursor = db.rawQuery(selectQuery, null);
            try {

                // looping through all rows and adding to list
                if (cursor.moveToFirst()) {
                    do {
                        itinerary obj = new itinerary(cursor.getString(0),cursor.getString(1),cursor.getDouble(2));
                        //only one column


                        //you could add additional columns here..
                        list.add(obj);
                    } while (cursor.moveToNext());
                }

            } finally {
                try { cursor.close(); } catch (Exception ignore) {}
            }

        } finally {
            try { db.close(); } catch (Exception ignore) {}
        }

        return list;
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 1000);
    }

}