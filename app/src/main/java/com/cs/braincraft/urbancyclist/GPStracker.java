package com.cs.braincraft.urbancyclist;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.location.LocationListener;
import android.provider.Settings;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by braincraft on 3/22/15.
 */
public class GPStracker extends Service implements LocationListener {

    private final Context context;

    boolean isGPSEnabled=false;
    boolean isNetworkEnabled=false;
    boolean canGetLocation=false;

    Location location;

    double latitude;
    double longitude;

    private static final long MIN_DISTANCE_FOR_CHANGE_FOR_UPDATES=10;
    private static final long MIN_TIME_BW_UPDATES=100;

    protected LocationManager locationManager;

    public GPStracker(Context context){
        this.context=context;
        getLocation();
    }

    double last_latitude=0;
    double last_longitude=0;
    private double distance=0;

    private Location getLocation() {
        try {
            locationManager = (LocationManager) context.getApplicationContext().getSystemService(LOCATION_SERVICE);
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {

            } else {
                if (isGPSEnabled==false)
                    this.canGetLocation = false;
                else {
                    this.canGetLocation=true;
                    if (isNetworkEnabled) {
                        locationManager.requestLocationUpdates(
                                LocationManager.NETWORK_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_FOR_CHANGE_FOR_UPDATES,
                                this
                        );


                        if (locationManager != null) {
                            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }

                    }
                    if (isGPSEnabled) {
                        this.canGetLocation = true;
                        if (location == null) {
                            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_FOR_CHANGE_FOR_UPDATES, this);
                            if (locationManager != null) {
                                location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                                if (location != null) {
                                    latitude = location.getLatitude();
                                    longitude = location.getLongitude();

                                }
                            }
                        }
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        last_latitude=latitude;
        last_longitude=longitude;
        return location;
    }


    public void stopUsingGPS(){
        if(locationManager!=null){
            locationManager.removeUpdates(GPStracker.this);

        }

    }

    public double getLatitude(){
        if(location!=null){
            latitude=location.getLatitude();
        }
        return latitude;
    }

    public double getLongitude(){
        if(location!=null){
            longitude=location.getLongitude();
        }
        return longitude;
    }

    public boolean canGetLocation(){
        return this.canGetLocation;
    }

    public void showSettingsAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle("GPS Settings");
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                context.startActivity(intent);
            }
        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            Location A = new Location("Point A");
            A.setLatitude(last_latitude);
            A.setLongitude(last_longitude);
            Location B = new Location("Point B");
            //B.setLatitude(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLatitude());
            //B.setLongitude(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLongitude());
            B.setLatitude(location.getLatitude());
            B.setLongitude(location.getLongitude());

            //distance=distance+A.distanceTo(B);
            this.distance = this.distance + distance_between(A, B);

            //last_latitude = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLatitude();
            //last_longitude = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLongitude();
            last_latitude=location.getLatitude();
            last_longitude=location.getLongitude();
            Toast.makeText(context, "Your Location is:\n(" + location.getLatitude() + "," + location.getLongitude() + ")\nDistance Travelled: " + distance_between(A, B), Toast.LENGTH_SHORT).show();
        } catch (Exception e){}
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(context, "GPS Signal Lost",Toast.LENGTH_SHORT).show();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    double distance_between(Location l1, Location l2)
    {

        double lat1=l1.getLatitude();
        double lon1=l1.getLongitude();
        double lat2=l2.getLatitude();
        double lon2=l2.getLongitude();
        double R = 6371; // km
        double dLat = (lat2-lat1)*Math.PI/180;
        double dLon = (lon2-lon1)*Math.PI/180;
        lat1 = lat1*Math.PI/180;
        lat2 = lat2*Math.PI/180;

        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double d = R * c;


        return d;
    }

    public void setDistance(double x){
        this.distance=x;
    }

    public double getDistance(){
        return this.distance;
    }


}